<?php
namespace Figured\Cowculator;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Figured\Cowculator\Calc
 */
class CalcTest extends TestCase
{
    /** @var Calc */
    protected $calc;

    protected function setUp()
    {
        parent::setUp();

        $this->calc = new Calc();
    }

    /**
     * @covers ::getScale()
     * @covers ::setScale()
     */
    public function testScale()
    {
        $baseNumber = '0.12345678901234567890';
        $this->assertEquals(14, $this->calc->getScale(), 'The default scale should be 14');

        $this->assertSame(
            '0.12345678901234',
            $this->calc->multiply($baseNumber, 1),
            'The returned string should have 14 decimals'
        );
        $this->assertSame(
            '0.00000000000000',
            $this->calc->add(0, 0),
            'The returned string should have 14 decimals'
        );

        $this->assertSame($this->calc, $this->calc->setScale(2), 'setScale should return the calc instance');
        $this->assertEquals(2, $this->calc->getScale(), 'The scale should be 2 after setting it to 2');

        $this->assertSame(
            '0.12',
            $this->calc->multiply($baseNumber, 1),
            'The returned string should have 2 decimals after setting the scale to 2'
        );
        $this->assertSame(
            '0.00',
            $this->calc->add(0, 0),
            'The returned string should have 2 decimals after setting the scale to 2'
        );
    }

    /**
     * @covers ::add()
     */
    public function testAdd()
    {
        $this->assertSame($this->normaliseExpectation(2), $this->calc->add(1, 1), '1 + 1 should be 2');
        $this->assertSame($this->normaliseExpectation(1), $this->calc->add(1, 0), '1 + 0 should be 1');
        $this->assertSame($this->normaliseExpectation(-1), $this->calc->add(-1, 0), '-1 + 0 should be -1');
        $this->assertSame($this->normaliseExpectation(0.3), $this->calc->add(0.1, 0.2), '0.1 + 0.2 should be 0.3');

        // Very small floating point number
        $this->assertSame(
            '0.00000000000878',
            $this->calc->add('0.000000000003233', '0.000000000005555'),
            '0.000000000003233 and 0.000000000005555 should be 0.00000000000878');

        // As above but negative
        $this->assertSame(
            '-0.00000000000878',
            $this->calc->add('-0.000000000003233', '-0.000000000005555'),
            '-0.000000000003233 and -0.000000000005555 should be 0.00000000000878');

        // Same very small number as exponential, note because we do not change scale these numbers
        // internally will be 0.00000000000323 and 0.00000000000556 which gives a slightly different result.
        $this->assertSame(
            '0.00000000000879',
            $this->calc->add('3.233E-12', '5.555E-12'),
            '3.233E-12 and 5.555E-12 should be 0.00000000000879');

        // As above but negative
        $this->assertSame(
            '-0.00000000000879',
            $this->calc->add('-3.233E-12', '-5.555E-12'),
            '-3.233E-12 and -5.555E-12 should be -0.00000000000879');

        // As above but negative + positive
        $this->assertSame(
            '-0.00000000000233',
            $this->calc->add('3.233E-12', '-5.555E-12'),
            '3.233E-12 and -5.555E-12 should be -0.00000000000233');

        // Same numbers as above, but we change the precision to the size of the original.
        $this->calc->setScale(15);
        $this->assertSame(
            '0.000000000008788',
            $this->calc->add('3.233E-12', '5.555E-12'),
            '0.000000000003233 and 0.000000000005555 should be 0.000000000008788');
    }

    /**
     * @covers ::subtract()
     */
    public function testSubtract()
    {
        $this->assertSame($this->normaliseExpectation(0), $this->calc->subtract(1, 1), '1 - 1 should be 0');
        $this->assertSame($this->normaliseExpectation(1), $this->calc->subtract(1, 0), '1 - 0 should be 1');
        $this->assertSame($this->normaliseExpectation(-1), $this->calc->subtract(-1, 0), '-1 - 0 should be -1');
        $this->assertSame($this->normaliseExpectation(0.1), $this->calc->subtract(0.3, 0.2), '0.3 - 0.2 should be 0.1');
        $this->assertSame('-0.00000000000233', $this->calc->subtract('3.233E-12', '5.555E-12'), '3.233E-12 - 5.555E-12 should be -0.00000000000233');
        // slightly different result becuase of scale conversion
        $this->assertSame('-0.00000000000232', $this->calc->subtract('0.000000000003233', '0.000000000005555'), '0.000000000003233 - 0.000000000005555 should be -0.00000000000232');
        // same result with
        $this->calc->setScale(15);
        $this->assertSame('-0.000000000002322', $this->calc->subtract('0.000000000003233', '0.000000000005555'), '0.00000000000323 - 0.00000000000555 should be -0.00000000000233');
    }

    /**
     * @covers ::multiply()
     * @cover ::phpScaleValue()
     */
    public function testMultiply()
    {
        $this->assertSame($this->normaliseExpectation(1), $this->calc->multiply(1, 1), '1 * 1 should be 1');
        $this->assertSame($this->normaliseExpectation(4), $this->calc->multiply(2, 2), '2 * 2 should be 4');
        $this->assertSame($this->normaliseExpectation(0), $this->calc->multiply(1, 0), '1 * 0 should be 0');
        $this->assertSame($this->normaliseExpectation(-1), $this->calc->multiply(-1, 1), '-1 * 1 should be -1');
        $this->assertSame($this->normaliseExpectation(0.02), $this->calc->multiply(0.1, 0.2), '0.1 * 0.2 should be 0.02');
        $this->assertSame('0.00000000000000', $this->calc->multiply('3.233E-12', '5.555E-12'), '3.233E-12 * 5.555E-12 should be 0.00000000000000');
        // These two lines are equivalent
        $this->assertSame('0.00000000001795', $this->calc->multiply('0.000003233', '0.000005555'), '0.000003233 * 0.000005555 should be 0.00000000001795');
        $this->assertSame('0.00000000001795', $this->calc->multiply('3.233E-6', '5.555E-6'), '3.233E-6 * 5.555E-6 should be 0.00000000001795');
    }

    /**
     * @covers ::divide()
     * @covers ::performNullableBinaryOperation()
     */
    public function testDivide()
    {
        $this->assertSame($this->normaliseExpectation(1), $this->calc->divide(1, 1), '1 / 1 should be 1');
        $this->assertSame($this->normaliseExpectation(1), $this->calc->divide(2, 2), '2 / 2 should be 1');
        $this->assertSame($this->normaliseExpectation(0), $this->calc->divide(0, 2), '0 / 2 should be 0');
        $this->assertSame($this->normaliseExpectation(-1), $this->calc->divide(-1, 1), '-1 / 1 should be -1');
        $this->assertSame($this->normaliseExpectation(0.5), $this->calc->divide(0.1, 0.2), '0.1 / 0.2 should be 0.5');
        // These two lines are equivalent
        $this->assertSame('0.58199819981998', $this->calc->divide('0.000003233', '0.000005555'), '0.000003233 / 0.000005555 should be 0.58199819981998');
        $this->assertSame('0.58199819981998', $this->calc->divide('3.233E-6', '5.555E-6'), '3.233E-6 / 5.555E-6 should be 0.58199819981998');
    }

    /**
     * @covers ::raiseByExponent()
     * @covers ::phpScaleValue()
     */
    public function testRaiseByExponent()
    {
        $this->assertSame($this->normaliseExpectation(1), $this->calc->raiseByExponent(0, 0), '0 ^ 0 should be 1');
        $this->assertSame($this->normaliseExpectation(0), $this->calc->raiseByExponent(0, 1), '0 ^ 1 should be 0');
        $this->assertSame($this->normaliseExpectation(1), $this->calc->raiseByExponent(1, 1), '1 ^ 1 should be 1');
        $this->assertSame($this->normaliseExpectation(8), $this->calc->raiseByExponent(8, 1), '8 ^ 1 should be 8');
        $this->assertSame($this->normaliseExpectation(32768), $this->calc->raiseByExponent(8, 5), '8 ^ 5 should be 32768');
        $this->assertSame($this->normaliseExpectation(-1), $this->calc->raiseByExponent(-1, 5), '-1 ^ 5 should be -1');
        $this->assertSame($this->normaliseExpectation(-125), $this->calc->raiseByExponent(-5, 3), '-5 ^ 3 should be -125');
        $this->assertSame($this->normaliseExpectation(-0.00032), $this->calc->raiseByExponent(-5, -5), '-5 ^ -5 should be -0.00032');
    }

    public function testArraySum()
    {
        $this->assertSame($this->normaliseExpectation(2), $this->calc->arraySum([1, 1]), '1 + 1 should be 2');
        $this->assertSame($this->normaliseExpectation(1), $this->calc->arraySum([1, 0]), '1 + 0 should be 1');
        $this->assertSame($this->normaliseExpectation(-1), $this->calc->arraySum([-1, 0]), '-1 + 0 should be -1');
        $this->assertSame($this->normaliseExpectation(0.3), $this->calc->arraySum([0.1, 0.2]), '0.1 + 0.2 should be 0.3');
        $this->assertSame($this->normaliseExpectation(11.1), $this->calc->arraySum([-3.1, 2, 3, 4, 5 + 0.2]), 'test more than 2 numbers');
    }

    /**
     * @covers ::compare()
     * @covers ::equalTo()
     * @covers ::lessThan()
     * @covers ::lessThanOrEqualTo()
     * @covers ::greaterThan()
     * @covers ::greaterThanOrEqualTo()
     */
    public function testComparisons()
    {
        // Equal to
        $this->assertSame(0, $this->calc->compare(0, 0), '0 should be equal to 0');
        $this->assertSame(0, $this->calc->compare(1, 1), '1 should be equal to 1');
        $this->assertSame(0, $this->calc->compare(1, 1.0), '1 should be equal to 1.0');
        $this->assertSame(0, $this->calc->compare(-1, -1.0), '-1 should be equal to -1.0');
        $this->assertSame(0, $this->calc->compare(0.5, 0.5), '0.5 should be equal to 0.5');
        $this->assertSame(0, $this->calc->compare(-0.5, -0.5), '-0.5 should be equal to -0.5');
        $this->assertTrue($this->calc->equalTo(0, 0), '0 should be equal to 0');
        $this->assertTrue($this->calc->equalTo(1, 1), '1 should be equal to 1');
        $this->assertTrue($this->calc->equalTo(1, 1.0), '1 should be equal to 1.0');
        $this->assertTrue($this->calc->equalTo(-1, -1.0), '-1 should be equal to -1.0');
        $this->assertTrue($this->calc->equalTo(0.5, 0.5), '0.5 should be equal to 0.5');
        $this->assertTrue($this->calc->equalTo(-0.5, -0.5), '-0.5 should be equal to -0.5');
        $this->assertTrue($this->calc->equalTo(0.000003233, '3.233E-6'), '0.000003233 should be equal to 3.233E-6');
        $this->assertTrue($this->calc->equalTo(0.000000000003233, '3.233E-12'), '0.000000000003233 should be equal to 3.233E-12');
        $this->assertTrue($this->calc->equalTo(-0.000000000005555, '-5.555E-12'), '-0.000000000005555 should be equal to -5.555E-12');

        // Less than
        $this->assertSame(-1, $this->calc->compare(0, 1), '0 should be less than 1');
        $this->assertSame(-1, $this->calc->compare(-1, 0), '-1 should be less than 0');
        $this->assertSame(-1, $this->calc->compare(1, 2.0), '1 should be less than 2.0');
        $this->assertSame(-1, $this->calc->compare(0.5, 0.6), '0.5 should be less than 0.6');
        $this->assertSame(-1, $this->calc->compare(-0.6, -0.5), '-0.6 should be less than -0.5');
        $this->assertSame(0, $this->calc->compare(0.03233, '3.233E-2'), '0.03233 should be equal to 3.233E-2');
        $this->assertTrue($this->calc->lessThan(0, 1), '0 should be less than 1');
        $this->assertTrue($this->calc->lessThan(-1, 0), '-1 should be less than 0');
        $this->assertTrue($this->calc->lessThan(1, 2.0), '1 should be less than 2.0');
        $this->assertTrue($this->calc->lessThan(0.5, 0.6), '0.5 should be less than 0.6');
        $this->assertTrue($this->calc->lessThan(-0.6, -0.5), '-0.6 should be less than -0.5');
        $this->assertTrue($this->calc->lessThan(0.03233, '3.334E-2'), '-0.6 should be less than -0.5');
        $this->assertFalse($this->calc->lessThan('3.334E-2', '3.334E-2'), '-0.6 should be less than -0.5');

        // Less than or equal to
        $this->assertTrue($this->calc->lessThanOrEqualTo(0, 1), '0 should be less than 1');
        $this->assertTrue($this->calc->lessThanOrEqualTo(0, 0), '0 should be equal to 0');
        $this->assertTrue($this->calc->lessThanOrEqualTo(-1, 0.0), '-1 should be less than 0.0');
        $this->assertTrue($this->calc->lessThanOrEqualTo(-1, -1.0), '-1 should be equal to -1.0');
        $this->assertTrue($this->calc->lessThanOrEqualTo(0.5, 0.6), '0.5 should be less than 0.6');
        $this->assertTrue($this->calc->lessThanOrEqualTo(0.5, 0.5), '0.5 should be equal to 0.5');
        $this->assertTrue($this->calc->lessThanOrEqualTo(-0.6, -0.5), '-0.6 should be less than -0.5');
        $this->assertTrue($this->calc->lessThanOrEqualTo(-0.5, -0.5), '-0.5 should be equal to -0.5');
        $this->assertTrue($this->calc->lessThanOrEqualTo('3.334E-2', '3.334E-2'), '3.334E-2 should be equal to 3.334E-2');

        // Greater than
        $this->assertSame(1, $this->calc->compare(1, 0), '1 should be greater than 0');
        $this->assertSame(1, $this->calc->compare(0, -1), '0 should be greater than -1');
        $this->assertSame(1, $this->calc->compare(2.0, 1), '2.0 should be greater than 1');
        $this->assertSame(1, $this->calc->compare(0.6, 0.5), '0.6 should be greater than 0.5');
        $this->assertSame(1, $this->calc->compare(-0.5, -0.6), '-0.5 should be greater than -0.6');
        $this->assertTrue($this->calc->greaterThan(1, 0), '1 should be greater than 0');
        $this->assertTrue($this->calc->greaterThan(0, -1), '0 should be greater than -1');
        $this->assertTrue($this->calc->greaterThan(2.0, 1), '2.0 should be greater than 1');
        $this->assertTrue($this->calc->greaterThan(0.6, 0.5), '0.6 should be greater than 0.5');
        $this->assertTrue($this->calc->greaterThan(-0.5, -0.6), '-0.5 should be greater than -0.6');
        $this->assertTrue($this->calc->greaterThan('3.434E-2','3.334E-2'), '3.434E-2 should be greater than 3.334E-2');
        $this->assertFalse($this->calc->greaterThan('3.334E-2','3.334E-2'), '3.334E-2 should not be greater than 3.334E-2');

        // Greater than or equal to
        $this->assertTrue($this->calc->greaterThanOrEqualTo(1, 0), '1 should be greater than 0');
        $this->assertTrue($this->calc->greaterThanOrEqualTo(0, 0), '0 should be equal to 0');
        $this->assertTrue($this->calc->greaterThanOrEqualTo(0.0, -1), '0.0 should be greater than -1');
        $this->assertTrue($this->calc->greaterThanOrEqualTo(-1, -1.0), '-1 should be equal to -1.0');
        $this->assertTrue($this->calc->greaterThanOrEqualTo(0.6, 0.5), '0.6 should be greater than 0.5');
        $this->assertTrue($this->calc->greaterThanOrEqualTo(0.5, 0.5), '0.5 should be equal to 0.5');
        $this->assertTrue($this->calc->greaterThanOrEqualTo(-0.5, -0.6), '-0.5 should be greater than -0.6');
        $this->assertTrue($this->calc->greaterThanOrEqualTo(-0.5, -0.5), '-0.5 should be equal to -0.5');
        $this->assertTrue($this->calc->greaterThanOrEqualTo('3.334E-2','3.334E-2'), '3.334E-2 should be equal to 3.334E-2');
        $this->assertTrue($this->calc->greaterThanOrEqualTo('3.434E-2','3.334E-2'), '3.434E-2 should be greater than 3.334E-2');
        $this->assertFalse($this->calc->greaterThanOrEqualTo('3.234E-2','3.334E-2'), '3.234E-2 should not be greater than 3.334E-2');
    }

    /**
     * @covers ::divide()
     * @covers ::divideNullable()
     */
    public function testDivideBy0()
    {
        $this->assertEquals(0, $this->calc->divide(1, 0), 'Safe division should return 0 when dividing by 0');

        try {
            $this->calc->divide(1, 0, false);
            $this->fail('Non-safe division by 0 should trigger a warning');
        } catch (\PHPUnit_Framework_Error_Warning $e) {
            $this->assertContains('Division by zero', $e->getMessage());
        }

        // Also test nullable non safe division by null as it's not tested elsewhere
        try {
            $this->calc->divideNullable(1, null, false, false);
            $this->fail('Non-safe nullable division by NULL should trigger a warning');
        } catch (\PHPUnit_Framework_Error_Warning $e) {
            $this->assertContains('Division by zero', $e->getMessage());
        }

        // We should ensure it works even when passing floats string
        $this->assertEquals(0, $this->calc->divide(1, '0.00000000000000'),
            'Safe division should return 0 when dividing by 0.00000000000000');

        try {
            $this->calc->divide(1, '0.00000000000000', false);
            $this->fail('Non-safe division by 00000000000000 should trigger a warning');
        } catch (\PHPUnit_Framework_Error_Warning $e) {
            $this->assertContains('Division by zero', $e->getMessage());
        }
    }

    /**
     * @dataProvider getInvalidNumbersForMethods
     *
     * @covers ::add()
     * @covers ::addNullable()
     * @covers ::subtract()
     * @covers ::subtractNullable()
     * @covers ::multiply()
     * @covers ::multiplyNullable()
     * @covers ::divide()
     * @covers ::divideNullable()
     */
    public function testInvalidNumbers(string $methodName, $value, bool $shouldFail, string $description)
    {
        if ($shouldFail) {
            try {
                $this->calc->$methodName($value, 1);
                $this->fail(
                    "$methodName should throw an InvalidArgumentException when '$value' is passed as the first param"
                );
            } catch (\InvalidArgumentException $e) {
                $this->assertContains(
                    "'$value'",
                    $e->getMessage(),
                    'The InvalidArgumentException should contain the invalid numeric string in the message'
                );
            }

            try {
                $this->calc->$methodName(1, $value);
                $this->fail(
                    "$methodName should throw an InvalidArgumentException when '$value' is passed as the second param"
                );
            } catch (\InvalidArgumentException $e) {
                $this->assertContains(
                    "'$value'",
                    $e->getMessage(),
                    'The InvalidArgumentException should contain the invalid numeric string in the message'
                );
            }
        } else {
            $this->calc->$methodName($value, 1);
            $this->calc->$methodName(1, $value);
        }
    }

    /**
     * @dataProvider getNotNullableMethods
     *
     * @covers ::addNullable()
     * @covers ::subtractNullable()
     * @covers ::multiplyNullable()
     * @covers ::divideNullable()
     * @covers ::performNullableBinaryOperation()
     */
    public function testNullHandling(
        string $methodName,
        bool $hasNullableMethod,
        string $otherOperand,
        string $zeroValueOperand1Null,
        string $zeroValueOperand2Null
    ) {
        try {
            $this->calc->$methodName(null, $otherOperand);
            $this->fail('A TypeError should be thrown when null is passed as the first operand');
        } catch (\TypeError $e) {
            $this->assertContains(
                'null given',
                $e->getMessage(),
                'The reason for the type error should be the null value'
            );
        }

        try {
            $this->calc->$methodName($otherOperand, null);
            $this->fail('A TypeError should be thrown when null is passed as the second operand');
        } catch (\TypeError $e) {
            $this->assertContains(
                'null given',
                $e->getMessage(),
                'The reason for the type error should be the null value'
            );
        }

        $nullableMethodName = $methodName . 'Nullable';

        if ($hasNullableMethod) {
            $this->assertNull(
                $this->calc->$nullableMethodName(null, $otherOperand, true),
                'Null should be returned by a nullable method when preserveNulls is TRUE and NULL is the first operand'
            );
            $this->assertEquals(
                $zeroValueOperand1Null,
                $this->calc->$nullableMethodName(null, $otherOperand, false),
                'Invalid value returned by a nullable method when preserveNulls is FALSE and NULL is the first operand'
            );
            $this->assertNull(
                $this->calc->$nullableMethodName($otherOperand, null, true),
                'Null should be returned by a nullable method when preserveNulls is TRUE and NULL is the second operand'
            );
            $this->assertEquals(
                $zeroValueOperand2Null,
                $this->calc->$nullableMethodName($otherOperand, null, false),
                'Invalid value returned by a nullable method when preserveNulls is FALSE and NULL is the second operand'
            );
        }
    }

    /**
     * @covers ::inflate()
     */
    public function testInflate()
    {
        $this->assertNull($this->calc->inflate(null));
        $this->assertSame(10000, $this->calc->inflate('1'));
        $this->assertSame(-10000, $this->calc->inflate('-1'));
        $this->assertSame(1, $this->calc->inflate('0.0001'));
        $this->assertSame(1, $this->calc->inflate('0.00005'));
        $this->assertSame(0, $this->calc->inflate('0.000049'));
    }

    /**
     * @covers ::deflate()
     */
    public function testDeflate()
    {
        $this->assertNull($this->calc->deflate(null));
        $this->assertSame($this->normaliseExpectation(1), $this->calc->deflate(10000));
        $this->assertSame($this->normaliseExpectation(-1), $this->calc->deflate(-10000));
        $this->assertSame($this->normaliseExpectation(0.0001), $this->calc->deflate(1));
        $this->assertSame($this->normaliseExpectation(0), $this->calc->deflate(0));
    }

    /**
     * @covers ::castFloatToInt()
     */
    public function testCastFloatToInt()
    {
        $this->assertSame(
            108300,
            $this->calc->castFloatToInt(108299.99999999997089616954326629638671875),
            'A number that is insignificantly less then the closest int should be rounded instead of casting'
        );

        $this->assertSame(
            0,
            $this->calc->castFloatToInt(0.9999),
            "A number that's slightly less then the closest int, but the difference is not insignificant should be cast"
        );
    }

    /**
     * @covers ::normaliseUserEnteredNumber()
     */
    public function testNormaliseUserEnteredNumber()
    {
        $this->assertSame('1.00', $this->calc->normaliseUserEnteredNumber(' 1.00'));
        $this->assertSame('1000.00', $this->calc->normaliseUserEnteredNumber(' 1,000.00 '));
    }

    /**
     * @dataProvider getInvalidUserInputs
     *
     * @covers ::normaliseUserEnteredNumber()
     */
    public function testNormaliseUserEnteredNumberInvalid(string $value)
    {
        try {
            $this->calc->normaliseUserEnteredNumber($value);
            $this->fail('An InvalidArgumentException should be thrown');
        } catch (\InvalidArgumentException $e) {
            $this->assertContains(
                "'$value'",
                $e->getMessage(),
                'The InvalidArgumentException should contain the invalid numeric string in the message'
            );
        }
    }

    /**
     * @covers ::requireNumericString()
     * @dataProvider getNumberValidations
     */
    public function testRequireNumericString($value, bool $shouldFail, string $description)
    {
        if ($shouldFail) {
            try {
                $this->calc->requireNumericString($value, 'test');
                $this->fail(
                    "An InvalidArgumentException should be thrown. Reason: $description"
                );
            } catch (\InvalidArgumentException $e) {
                $this->assertContains(
                    "'$value'",
                    $e->getMessage(),
                    'The InvalidArgumentException should contain the invalid numeric string in the message'
                );
                $this->assertContains(
                    "'test'",
                    $e->getMessage(),
                    'The InvalidArgumentException should contain the parameter name in the message'
                );
            }

            try {
                $this->calc->requireNumericString($value);
                $this->fail(
                    "An InvalidArgumentException should be thrown. Reason: $description"
                );
            } catch (\InvalidArgumentException $e) {
                $this->assertContains(
                    "'$value'",
                    $e->getMessage(),
                    'The InvalidArgumentException should contain the invalid numeric string in the message'
                );
            }
        } else {
            $this->calc->requireNumericString($value);
        }
    }

    /**
     * @covers ::isNumeric()
     * @dataProvider getNumberValidations
     */
    public function testIsNumericString($value, bool $shouldFail, string $description)
    {
        $this->assertSame(!$shouldFail, $this->calc->isNumeric($value), $description);
    }

    /**
     * @covers ::trimDecimalPrecision()
     */
    public function testTrimDecimalPrecision()
    {
        $this->assertSame(1.0, $this->calc->trimDecimalPrecision(1));
        $this->assertSame(1.0, $this->calc->trimDecimalPrecision(1.0));
        $this->assertSame(1.1111, $this->calc->trimDecimalPrecision(1.1111111111));
        $this->assertSame(999.9999, $this->calc->trimDecimalPrecision(999.9999999999));
        $this->assertSame(77.77, $this->calc->trimDecimalPrecision(77.7777777777, 2));
        $this->assertSame(3.33300, $this->calc->trimDecimalPrecision(3.3330000, 5));
        $this->assertSame(-1.111, $this->calc->trimDecimalPrecision(-1.1111111111111111111, 3));
        $this->assertSame(8888888888.88, $this->calc->trimDecimalPrecision(8888888888.888888, 2));

        // testing numbers less than 0
        $this->assertSame(0.0004, $this->calc->trimDecimalPrecision(0.0004000088, 6));
        $this->assertSame(0.0004000056, $this->calc->trimDecimalPrecision(0.0004000056, 10));
        $this->assertSame(0.0000000088, $this->calc->trimDecimalPrecision(0.0000000088, 10));

        // testing numbers passed in the scientific notation
        $this->assertSame(0.00000000058, $this->calc->trimDecimalPrecision(5.8e-10, 10));
        $this->assertSame(0.00000005, $this->calc->trimDecimalPrecision(5.8e-8, 8));
        $this->assertSame(-0.00000007, $this->calc->trimDecimalPrecision(-7.3e-8, 8));
        $this->assertSame(0.0, $this->calc->trimDecimalPrecision(5.8e-8, 2));
        $this->assertSame(580.0, $this->calc->trimDecimalPrecision(5.8e+2, 2));
    }

    public function getInvalidNumbersForMethods(): array
    {
        $result = [];

        foreach ($this->getNotNullableMethods() as $methodParams) {
            $methodNames = [$methodParams[0]];
            if ($methodParams[1]) {
                $methodNames[] = $methodParams[0] . 'Nullable';
            }

            foreach ($methodNames as $name) {
                foreach ($this->getNumberValidations() as $validation) {
                    $result[] = array_merge([$name], $validation);
                }
            }
        }

        return $result;
    }

    public function getInvalidUserInputs()
    {
        return [
            [''],
            [' '],
            ['gasgg'],
            ['523sg'],
            ['1.2.3'],
            ['x123'],
        ];
    }

    public function getNumberValidations()
    {
        return [
            [1, false, 'Ints are valid'],
            [0.1, false, 'Floats are valid'],
            ['1', false, 'Ints as strings are valid'],
            ['1.0', false, 'Floats as strings are valid'],
            ['-1.0', false, 'Negative numbers are valid'],
            ['01.0', false, 'Leading zeroes are valid (NOT OCTAL!)'],
            ['', true, 'Empty strings are not valid'],
            [' -1.0', true, 'Leading spaces are not valid'],
            ['1.0 ', true, 'Trailing spaces are not valid'],
            ['1.0x', true, 'Trailing text is not valid'],
            ['1.0.0', true, 'Multiple decimals are not valid'],
            ['x1', true, 'Leading text is not valid'],
            ['0x1', true, 'Hex notation is not valid'],
            ['0b1', true, 'Binary notation is not valid'],
            ['test', true, 'Text is not valid'],
        ];
    }

    /**
     * methodName, hasNullableMethod, otherOperand, zeroValueOperand1Null, zeroValueOperand2Null
     *
     * @return array
     */
    public function getNotNullableMethods(): array
    {
        return [
            ['add', true, 1, 1, 1],
            ['subtract', true, 1, -1, 1],
            ['multiply', true, 1, 0, 0],
            ['divide', true, 1, 0, 0],
            ['compare', false, 0, 0, 0],
        ];
    }

    protected function normaliseExpectation(string $result): string
    {
        $decimalPosition = strpos($result, '.');
        if (false === $decimalPosition) {
            $decimalPosition = strlen($result);
            $result .= '.';
        }

        return str_pad($result, $this->calc->getScale() + $decimalPosition + 1, '0');
    }
}

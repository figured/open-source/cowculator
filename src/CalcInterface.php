<?php
namespace Figured\Cowculator;

use InvalidArgumentException;

interface CalcInterface
{
    public const INFLATE_MULTIPLIER = 10000;

    public const CAST_PRECISION_THRESHOLD = 0.000001;

    public const DECIMAL_PRECISION_DEFAULT = 4;

    // max number before Calc inflate function break
    public const MAX_CALCULABLE_NUMBER = 999999999999;

    /**
     * Gets the scale parameter's value that's passed to the BC functions
     *
     * @return int
     */
    public function getScale(): int;

    /**
     * Sets the scale parameter's value that's passed to the BC functions
     *
     * @return self
     */
    public function setScale(int $scale): self;

    /**
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function add(string $a, string $b): string;

    /**
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function subtract(string $a, string $b): string;

    /**
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function multiply(string $a, string $b): string;

    /**
     * Returns operand a divided by b. Optionally prevents division by zero errors as well by returning 0 if b is 0
     *
     * @param string $a
     * @param string $b
     * @param bool   $safe Whether 0 divisor should result in an error or 0
     *
     * @return string
     *
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function divide(string $a, string $b, bool $safe = true): string;

    /**
     * Raise a number to the power exponent (8^3 = 8*8*8 = 512)
     *
     * @param string $number
     * @param string $exponent
     *
     * @return string
     *
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function raiseByExponent(string $number, string $exponent): string;

    /**
     * @param array $arr
     *
     * @return string
     *
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function arraySum(array $arr): string;

    /**
     * @param string $a
     * @param string $b
     *
     * @return int
     *
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function compare(string $a, string $b): int;

    /**
     * @param string $a
     * @param string $b
     *
     * @return bool
     *
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function lessThan(string $a, string $b): bool;

    /**
     * @param string $a
     * @param string $b
     *
     * @return bool
     *
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function equalTo(string $a, string $b): bool;

    /**
     * @param string $a
     * @param string $b
     *
     * @return bool
     *
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function greaterThan(string $a, string $b): bool;

    /**
     * @param string $a
     * @param string $b
     *
     * @return bool
     *
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function greaterThanOrEqualTo(string $a, string $b): bool;

    /**
     * @param string $a
     * @param string $b
     *
     * @return bool
     *
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function lessThanOrEqualTo(string $a, string $b): bool;

    /**
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function addNullable(?string $a, ?string $b, bool $preserveNulls = false): ?string;

    /**
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function subtractNullable(?string $a, ?string $b, bool $preserveNulls = false): ?string;

    /**
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function multiplyNullable(?string $a, ?string $b, bool $preserveNulls = false): ?string;

    /**
     * @see Calc::divide()
     *
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function divideNullable(?string $a, ?string $b, bool $preserveNulls = false, bool $safe = true): ?string;

    /**
     * Multiplies the specified number with the inflation multiplier and returns it as an int
     *
     * @param string $number
     *
     * @return int|null
     */
    public function inflate(string $number): ?int;

    /**
     * Takes a value that was already multiplied by the inflation multiplier and recovers the original value
     *
     * @param int|null $number
     *
     * @return string|null
     */
    public function deflate(?int $number): ?string;

    /**
     * Casts the specified float to an int
     *
     * If the original number is an insignificant amount less then the closest int, then round it instead of casting to
     * prevent a number, which may be produced by calculations that are affected float precision issues,
     * like 5.99999999999999542 being cast to 5 instead of 6.
     *
     * If your number is coming directly from user input, do a regular cast instead of using this method.
     *
     * @param float $float
     *
     * @return int
     */
    public function castFloatToInt(float $float): int;

    /**
     * Normalises any user provided number to a number string. If the string can't be normalised it throws an exception
     *
     * This currently will not work properly with non-english locales, as it removes comas from the string (to clean up
     * any thousands separator), but most languages use decimal comas instead of decimal points. We need to adjust this
     * method to use the locale setting when we start supporting other locales, and we actually set the locale by the
     * user's account.
     *
     * @param string $string
     *
     * @return string
     *
     * @throws InvalidArgumentException If the string can't be normalised
     */
    public function normaliseUserEnteredNumber(string $string): string;

    /**
     * Requires the passed parameter to be a numeric string, or throws an exception otherwise
     *
     * @param string      $number
     * @param string|null $parameterName If
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function requireNumericString(string $number, string $parameterName = null): void;

    /**
     * Returns TRUE if the specified string is a valid numeric string
     *
     * @param string $number
     *
     * @return bool
     */
    public function isNumeric(string $number): bool;

    /**
     * Returns a trimmed non-rounded float value to a specified precision
     *
     * @param float $float
     * @param int $precision
     * @return float
     */
    public function trimDecimalPrecision(float $float, int $precision = CalcInterface::DECIMAL_PRECISION_DEFAULT): float;
}

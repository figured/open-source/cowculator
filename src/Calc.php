<?php
namespace Figured\Cowculator;

use InvalidArgumentException;

class Calc implements CalcInterface
{
    /**
     * The scale parameter to pass to the BC functions
     *
     * @var int
     */
    protected $scale = 14;

    /**
     * Gets the scale parameter's value that's passed to the BC functions
     *
     * @return int
     */
    public function getScale(): int
    {
        return $this->scale;
    }

    /**
     * Sets the scale parameter's value that's passed to the BC functions
     *
     * @return CalcInterface
     */
    public function setScale(int $scale): CalcInterface
    {
        $this->scale = $scale;

        return $this;
    }

    public function add(string $a, string $b): string
    {
        list($a, $b) = $this->ensureBcmathSafeArgs([$a, $b]);

        return bcadd($a, $b, $this->scale);
    }

    public function subtract(string $a, string $b): string
    {
        list($a, $b) = $this->ensureBcmathSafeArgs([$a, $b]);

        return bcsub($a, $b, $this->scale);
    }

    public function multiply(string $a, string $b): string
    {
        list($a, $b) = $this->ensureBcmathSafeArgs([$a, $b]);

        $result = bcmul($a, $b, $this->scale);

        return $this->phpScaleValue($result);
    }

    /**
     * Returns operand a divided by b. Optionally prevents division by zero errors as well by returning 0 if b is 0
     *
     * @param string $a
     * @param string $b
     * @param bool   $safe Whether 0 divisor should result in an error or 0
     *
     * @return string
     *
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function divide(string $a, string $b, bool $safe = true): string
    {
        list($a, $b) = $this->ensureBcmathSafeArgs([$a, $b]);

        if ($safe && 0 == $b) {
            // Ensure that we are returning the number with the required scale
            return $this->add(0, 0);
        }

        return bcdiv($a, $b, $this->scale);
    }

    public function raiseByExponent(string $number, string $exponent): string
    {
        list($number, $exponent) = $this->ensureBcmathSafeArgs([$number, $exponent]);

        $result = bcpow($number, $exponent, $this->scale);

        return $this->phpScaleValue($result);
    }

    /**
     * Until PHP 7.3 some bc methods didn't return trailing zeroes up to the specified scale, so this fixes it
     *
     * @see https://www.php.net/manual/en/function.bcmul.php#refsect1-function.bcmul-notes
     *
     * @param string $number
     * @return string
     */
    private function phpScaleValue(string $number): string
    {
        if (PHP_MAJOR_VERSION < 7 || (PHP_MAJOR_VERSION == 7 && PHP_MINOR_VERSION < 3)) {
            $decimalPosition = strpos($number, '.');
            if (false === $decimalPosition) {
                $decimalPosition = strlen($number);
                $number .= '.';
            }

            return str_pad($number, $this->scale + $decimalPosition + 1, '0');
        }

        return $number;
    }

    public function arraySum(array $arr): string
    {
        return array_reduce($arr, [$this, 'add'], 0);
    }

    public function compare(string $a, string $b): int
    {
        list($a, $b) = $this->ensureBcmathSafeArgs([$a, $b]);

        return $this->inflate($a) <=> $this->inflate($b);
    }

    public function lessThan(string $a, string $b): bool
    {
        return $this->compare($a, $b) === -1;
    }

    public function equalTo(string $a, string $b): bool
    {
        return $this->compare($a, $b) === 0;
    }

    public function greaterThan(string $a, string $b): bool
    {
        return $this->compare($a, $b) === 1;
    }

    public function greaterThanOrEqualTo(string $a, string $b): bool
    {
        $result = $this->compare($a, $b);

        return $result === 1 || $result === 0;
    }

    public function lessThanOrEqualTo(string $a, string $b): bool
    {
        $result = $this->compare($a, $b);

        return $result === -1 || $result === 0;
    }

    public function addNullable(?string $a, ?string $b, bool $preserveNulls = false): ?string
    {
        return $this->performNullableBinaryOperation([$this, 'add'], $a, $b, $preserveNulls);
    }

    public function subtractNullable(?string $a, ?string $b, bool $preserveNulls = false): ?string
    {
        return $this->performNullableBinaryOperation([$this, 'subtract'], $a, $b, $preserveNulls);
    }

    public function multiplyNullable(?string $a, ?string $b, bool $preserveNulls = false): ?string
    {
        return $this->performNullableBinaryOperation([$this, 'multiply'], $a, $b, $preserveNulls);
    }

    /**
     * @see Calc::divide()
     *
     * @throws InvalidArgumentException If either of the operands are not numeric strings
     */
    public function divideNullable(?string $a, ?string $b, bool $preserveNulls = false, bool $safe = true): ?string
    {
        return $this->performNullableBinaryOperation(
            function ($a, $b) use ($safe) {
                return $this->divide($a, $b, $safe);
            },
            $a,
            $b,
            $preserveNulls
        );
    }

    /**
     * Multiplies the specified number with the inflation multiplier and returns it as an int
     *
     * @param string|null $number
     *
     * @return int|null
     */
    public function inflate(?string $number): ?int
    {
        if (null === $number) {
            return null;
        }

        return round($this->multiplyNullable($number, self::INFLATE_MULTIPLIER, true));
    }

    /**
     * Takes a value that was already multiplied with the inflation multiplier and recovers the original value
     *
     * @param int|null $number
     *
     * @return string|null
     */
    public function deflate(?int $number, bool $safeDivide = false): ?string
    {
        return $this->divideNullable($number, self::INFLATE_MULTIPLIER, true, $safeDivide);
    }

    /**
     * Casts the specified float to an int
     *
     * If the original number is an insignificant amount less then the closest int, then round it instead of casting to
     * prevent a number, which may be produced by calculations that are affected float precision issues,
     * like 5.99999999999999542 being cast to 5 instead of 6.
     *
     * If your number is coming directly from user input, do a regular cast instead of using this method.
     *
     * @param float $float
     *
     * @return int
     */
    public function castFloatToInt(float $float): int
    {
        $int = (int) $float;

        return $int < $float && abs($float - round($float)) < self::CAST_PRECISION_THRESHOLD ? round($float) : $int;
    }

    /**
     * Normalises any user provided number to a number string. If the string can't be normalised it throws an exception
     *
     * This currently will not work properly with non-english locales, as it removes comas from the string (to clean up
     * any thousands separator), but most languages use decimal comas instead of decimal points. We need to adjust this
     * method to use the locale setting when we start supporting other locales, and we actually set the locale by the
     * user's account.
     *
     * @param string $string
     *
     * @return string
     *
     * @throws InvalidArgumentException If the string can't be normalised
     *
     * @todo this does not play nicely with non-english locales
     */
    public function normaliseUserEnteredNumber(string $string): string
    {
        // Remove thousands separator and space from the string
        $number = trim(str_replace([',', ' '], '', $string));

        // Verify the number is a valid number
        if (!$this->isNumeric($number)) {
            throw new InvalidArgumentException(
                "Failed to normalise the number '$string', as the normalised version '$number' is not valid"
            );
        }

        return $number;
    }

    /**
     * Requires the passed parameter to be a numeric string, or throws an exception otherwise
     *
     * @param string      $number
     * @param string|null $parameterName If
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function requireNumericString(string $number, string $parameterName = null): void
    {
        if (!$this->isNumeric($number)) {
            throw new InvalidArgumentException(
                $parameterName === null
                    ? "The string '$number' is not a valid numeric string"
                    : "The parameter passed as '$parameterName' with the value '$number' is not a valid numeric string'"
            );
        }
    }

    /**
     * Returns TRUE if the specified string is a valid numeric string
     *
     * @param string $number
     *
     * @return bool
     */
    public function isNumeric(string $number): bool
    {
        return is_numeric($number) && strpos($number, ' ') !== 0;
    }

    /**
     * Performs the specified operation on the operands, converting NULL to 0, or returning NULL
     *
     * @param callable    $operation     The operation to perform
     * @param null|string $a             The first operand
     * @param null|string $b             The second operand
     * @param bool        $preserveNulls If TRUE, NULL will be returned if either operand is NULL
     *
     * @return null|string
     */
    protected function performNullableBinaryOperation(
        callable $operation,
        ?string $a,
        ?string $b,
        bool $preserveNulls = false
    ): ?string {
        if (null === $a) {
            if ($preserveNulls) {
                return null;
            }

            $a = 0;
        }

        if (null === $b) {
            if ($preserveNulls) {
                return null;
            }

            $b = 0;
        }

        return $operation($a, $b);
    }

    /**
     * Returns a trimmed non-rounded float value to a specified precision
     *
     * @param float $float
     * @param int $precision
     * @return float
     */
    public function trimDecimalPrecision(float $float, int $precision = CalcInterface::DECIMAL_PRECISION_DEFAULT): float
    {
        $precisionInflated = (int) str_pad('1', $precision + 1, '0');

        $float = $this->convertScientificNotationToString((string)$float);

        return floor(bcmul($float, $precisionInflated)) / $precisionInflated;
    }

    /**
     * Return a string representation of a float number even if it passed in the scientific notation
     *
     * @param string $number
     * @return string
     */
    protected function convertScientificNotationToString(string $number): string
    {
        if ($this->isScientificNotation($number)) {
            return number_format($number, $this->getScale(), localeconv()['decimal_point'], '');
        }

        return $number;
    }

    /**
     * Return true if the number is a string in floating point notation.
     *
     * @param string $number
     * @return bool
     */
    private function isScientificNotation(string $number): bool
    {
        return false !== stripos($number, 'E') && preg_match('/(-?(\d+\.)?\d+)E([+-]?)(\d+)/i', $number);
    }

    /**
     * Helper function to ensure args passed to functions are in correct string floating point format for
     * bcmath functions.
     *
     * Correct:   0.00000000000000032432555555
     * Incorrect: 3.2432555555E-16
     *
     * Previously to PHP 7.4 this would fail silently and assign 0 to the number. It now throws an exception.
     *
     * @param array $args
     * @return array
     */
    private function ensureBcmathSafeArgs(array $args): array
    {
        foreach ($args as $key => $arg) {
            $this->requireNumericString($arg);
            $args[$key] = $this->convertScientificNotationToString($arg);
        }
        return $args;
    }
}

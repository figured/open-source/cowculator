# Cowculator

[![pipeline status](https://gitlab.figured-dev.com/FiguredLimited/cowculator/badges/master/pipeline.svg)](https://gitlab.figured-dev.com/FiguredLimited/cowculator/commits/master)
[![coverage report](https://gitlab.figured-dev.com/FiguredLimited/cowculator/badges/master/coverage.svg)](https://gitlab.figured-dev.com/FiguredLimited/cowculator/commits/master)

Helper library for working with floating point numbers. Wraps around the `bc` functions.

## Tests

A docker compose file has been provided to make running tests locally easier.

### To run tests

Start docker: `docker-compose up`  
Shell into running image: `docker exec -it cowculator_php_1 bash`  
Composer: `composer install -n --prefer-dist`  
Run tests: `vendor/bin/phpunit --coverage-text --colors=never`

